# Basic data model.

We start with a statement. A statement *must* have an ID and a type, and associate either topics or literals to various roles. There must be at least one topic. 

Question: why literals? We could treat literals as topics (using their IPLD hash value as URN) with a `@value` tag. But in many cases, using the literal itself is going to be lighter and more legible; we must understand that a literal can be interchanged with its topic reference.

I am adopting the RDF convention that non-literal topic references, including types, are URIs, and can be written as CURIEs.
I know some of you like using UUIDs as topic identifiers; UUIDs can be expressed as URNs.

It makes perfect sense for the statement ID to be the IPLD hash of the statement itself (sans the ID.) In that case, we need to normalize the literal references to be either value-based or reference-based. (Check if and how IPLD handles this.) I will not make it mandatory, but that hash must always be easily accessible, as it will be used in indexing.

Example of an attribute statement:

```json
{
    "@id": "ipfs://QmT5NvUtoM5nWFfrQdVrFtvGfKFmG7AHE8P34isapyhCxX",
    "@type": "hk:attribute",
    "rdf:subject": "urn:isbn:0-395-36341-1",
    "rdf:predicate": "dc:date",
    "rdf:object": {
        "@id": "ipfs://QmDb5678eoM5nWFfrQdVrFtvGfKFmG7AHE8P34isapy7Xi",
        "@type": "xs:date",
        "@value": "2000-12-01"
    }
}
```

Which could be abusively abbreviated to

```json
{
    "@id": "ipfs://QmT5NvUtoM5nWFfrQdVrFtvGfKFmG7AHE8P34isapyhCxX",
    "@type": "hk:attribute",
    "rdf:subject": "urn:isbn:0-395-36341-1",
    "rdf:predicate": "dc:date",
    "rdf:object": {
        "@value": "2000-12-01"
    }
}
```

Example of an association statement:

```json
{
    "@id": "ipfs://Qm67890toM5nWFfrQdVrFtvGfKFmG7AHE8P34isapyhxY",
    "@type": "hk:association",
    "rdf:predicate": "eg:assoc_type",
    "eg:role1": [
        "eg2:topic1",
        "urn:isbn:0-395-36341-1"
    ],
    "eg:role2": "eg2:topic2",
    "eg:role3": {
        "@value": "Some literal string",
        "lang": "en"
    }
}
```

so really, an attribute statement is just an association with designated roles `rdf:subject`, `rdf:object`.

Now, this statement is made as part of an event:

```json
{
    "@id": "mystream:56789",
    "@type": "hk:stmt_event",
    "hk:when": "2020-08-09T10:02:67.4567Z",
    "hk:who": "did:someone_or_other",
    "hk:stream": "http://realm.example.com/stream_endpoint",
    "hk:evt_invalidates": ["mystream:56721", "mystream:56110"],
    "hk:evt_depends": ["mystream:56368", "mystream:56788"],
    "hk:parent_event": "mystream:56787",
    "hk:evt_stmt": {
        "@id": "ipfs://Qm67890toM5nWFfrQdVrFtvGfKFmG7AHE8P34isapyhxY",
        "@type": "hk:association",
        "rdf:predicate": "hk:assoc_type",
        "eg:role1": [
            "eg2:topic1",
            "urn:isbn:0-395-36341-1"
        ],
        "eg:role2": "eg2:topic2",
        "eg:role3": {
            "@value": "Some literal string",
            "lang": "en"
        }
    }
}
```

The stream endpoint allows getting individual events, subscribtions, etc. The stream information itself needs to state who is in charge of the stream (individual or collective.), whether the stream is based on (forks) a previous stream, and at which point; whether the stream is persistent at this endpoint or maybe counts on another storage unit for persistence; etc.
(Backref for forks)
If we're allowing stream rewriting, it means we need to specify if this stream is rewritten and where.

When asking an endpoint for a snapshot of an topic, we should get statement events indexed by statement type and role.


```json
{
    "@id": "ipfs://....",
    "@type": "hk:snapshot",
    "hk:stream": "http://realm.example.com/stream_endpoint",
    "hk:when": "2020-08-09T10:02:67.4567Z",
    "hk:last_event": "http://realm.example.com/stream_endpoint#56789",
    "rdf:subject": "urn:isbn:0-395-36341-1",
    "eg:by_stmt_type": {
        "eg:assoc_type": [
            {
                "@id": "mystream:56789",
                "@type": "hk:stmt_event",
                "hk:when": "2020-08-09T10:02:67.4567Z",
                "hk:who": "did:someone_or_other",
                "hk:stream": "http://realm.example.com/stream_endpoint",
                "hk:evt_invalidates": ["mystream:56721", "mystream:56110"],
                "hk:evt_depends": ["mystream:56368", "mystream:56788"],
                "hk:parent_event": "mystream:56787",
                "hk:evt_stmt": {
                    "@id": "ipfs://Qm67890toM5nWFfrQdVrFtvGfKFmG7AHE8P34isapyhxY",
                    "@type": "hk:association",
                    "rdf:predicate": "hk:assoc_type",
                    "eg:role1": [
                        "eg2:topic1",
                        "urn:isbn:0-395-36341-1"
                    ],
                    "eg:role2": "eg2:topic2",
                    "eg:role3": {
                        "@value": "Some literal string",
                        "@lang": "en"
                    }
                }
            }
        ]
        "dc:date": [
            {
                ...
                "@type": "hk:stmt_event",
                "hk:evt_stmt": {
                    "@id": "ipfs://QmT5NvUtoM5nWFfrQdVrFtvGfKFmG7AHE8P34isapyhCxX",
                    "@type": "hk:attribute",
                    "rdf:subject": "urn:isbn:0-395-36341-1",
                    "rdf:predicate": "dc:date",
                    "rdf:object": {
                        "@value": "2000-12-01"
                    }
                }
            }
        ]
    }
}
```

(It would be possible to further index the list of events by role, to be considered.)

What is important is that the snapshot contains event references, so it is possible for client to refer to those events when sending new events that might invalidate them.

Note: Invalidated events are not included in the snapshot.

Note2: When is it valid to send references instead of values in a snapshot?  Again, think of how IPLD handles this. But I think the point of IPLD is to be substantial.



