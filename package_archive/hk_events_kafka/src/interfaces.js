export type kId = string; // topic id. We could use ints.

export interface BaseEvent {
  id: ?kId
  ts: ?Date
  keyId: ?kId
}

export interface IEventSource {
  eventById(id: kId): Promise<?BaseEvent>;
  eventsStartingFrom(eventId: ?kId): AsyncGenerator<BaseEvent, void, void>;
  nextEventId(): Promise<?kId>;
}

export interface IEventStream extends IEventSource {
  setCallback(cb: IRawEventCallbackFn): void;
  clearCallback(): void;
  appendEvent(event: BaseEvent): Promise<void>;
  resetEvents(until_event: ?kId): void;
}

export type IEventStreamSpec = { id: kName, type: string, data: string, prefix: kId, url: URLType }
type IEventStreamFactoryFn = (spec: IEventStreamSpec, nocreate: ?boolean) => Promise<?IEventStream>;
type IRawEventCallbackFn = (ev: Object) => Promise<void>;

interface IRealmRegistry {
  registerEventStreamFactory(type: string, f: IEventStreamFactoryFn): void;
}
