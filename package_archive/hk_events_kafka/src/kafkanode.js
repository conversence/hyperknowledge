//@flow
import type {kId, BaseEvent, IEventSource, IEventStream, IRealmRegistry, IEventStreamSpec, IRawEventCallbackFn } from "./interfaces"
var kafka = require('kafka-node');
const util = require('util');
const exec = util.promisify(require('child_process').exec);

class PrePromise<T> {
  promise: Promise<T>
  resolve: (value: T) => any
  error: (error: any) => any
  constructor() {
    this.promise = new Promise((resolve, error)=>{
      this.resolve = resolve
      this.error = error
    })
  }
}

class KafkaReaderThread {
  ks: KafkaSource
  id: kId
  consumer: any
  kafkaClient: any
  promise: PrePromise<?Object>

  constructor(ks: KafkaSource, id: string, group: string) {
    this.id = id
    this.ks = ks
    const offsetData = ks.idToOffset(id)
    this.promise = new PrePromise<?Object>()
    this.consumer = this.makeConsumer(group)
  }

  makeConsumer(group) {
    const ks = this.ks
    const id = this.id
    this.kafkaClient = ks.getKafkaClient()
    const offsetData = ks.idToOffset(id)
    return new kafka.Consumer(
      this.kafkaClient, [offsetData], {
      kafkaHost: ks.kafkaHosts[0],
      id: ks.baseGroup,
      autoCommit: true,
      fromOffset: true,
      groupId: group,
    })
  }

  setOffset() {
    const offsetData = this.ks.idToOffset(this.id)
    this.consumer.setOffset(offsetData.topic, offsetData.partition, offsetData.offset)
  }

  stop(cb) {
    this.consumer.close(true, ()=>{
      this.promise.error('stopped')
      console.log('stopped')
      cb(this)
    })
  }

}

class RandomAccessReaderThread extends KafkaReaderThread {
  active: boolean

  constructor(ks: KafkaSource, id: string, group_number) {
    super(ks, id, ks.baseGroup+"_r"+group_number)
    this.active = true
    this.consumer.on('message', (message)=>{
      if (!this.active) {
        this.consumer.pause()
        return
      }
      const id = this.ks.offsetToId(message.partition, message.offset)
      if (id == this.id) {
        this.active = false
        this.consumer.pause()
        const payload = JSON.parse(message.value.toString())
        payload['@id'] = id
        payload.ts = new Date(message.timestamp)
        this.promise.resolve(payload)
      } else {
        console.warn("got id: "+id)
      }
    })
    this.consumer.on('offsetOutOfRange', (err)=>{
      console.warn('offsetOutOfRange')
      this.consumer.pause()
      if (this.active) {
        this.promise.resolve(null)
        this.active = false
      }
    })
    this.consumer.on('error', (error)=>{
      console.error(error)
      this.consumer.pause()
      if (this.active) {
        this.promise.error(error)
        this.active = false
      }
    })
    this.setOffset()
  }

  async eventById(id: kId): Promise<Object> {
    if (id != this.id) {
      if (this.active) {
        throw new Error("Double call")
      } else {
        this.id = id
        this.active = true
        this.promise = new PrePromise<?Object>()
        this.consumer.resume()
        this.setOffset()
      }
    }
    return await this.promise.promise
  }

}

class OffsetReader {
  ks: KafkaSource
  offsetClientPromise: Promise<any>
  knownEnds: ?Array<number>
  constructor(ks: KafkaSource, kafkaClient: any) {
    this.ks = ks
    const offsetClient = new kafka.Offset(kafkaClient)
    const offsetReady = new Promise((r)=>{
      offsetClient.on('ready', ()=>{r()})
    })
    const offsetConnected = new Promise((r)=>{
      offsetClient.on('connect', ()=>{r()})
    })
    this.offsetClientPromise = Promise.all(
      [offsetReady, offsetConnected]).then(() => offsetClient)
  }
  async calcLastOffsets(): Promise<Array<number>> {
    const client = await this.getClient()
    return new Promise((r, err)=>{
      client.fetchLatestOffsets([this.ks.topic], (error, offsets)=>{
        if (error)
          err(error)
        else {
          const knownEnds = []
          const offsetsT = offsets[this.ks.topic]
          const maxPartitions = Math.max(...Object.keys(offsetsT))
          for (let i=0; i <= maxPartitions; i++) {
            knownEnds.push(offsetsT[i] || 0)
          }
          r(knownEnds)
        }
      })
    })
  }

  async lastOffsets(recalc?: boolean): Promise<Array<number>> {
    let knownEnds = this.knownEnds
    if (recalc || !knownEnds) {
      knownEnds = await this.calcLastOffsets()
      this.knownEnds = knownEnds
    }
    return knownEnds
  }



  async getClient() {
    return await this.offsetClientPromise
  }

}

class StreamReaderThread extends KafkaReaderThread {
  values: Array<any>
  lastSeen: Array<number>
  offsetReader: OffsetReader
  wait: boolean
  ended: boolean
  constructor(ks: KafkaSource, id: string, group_number: number, wait?: boolean) {
    super(ks, id, ks.baseGroup+"_s"+group_number)
    this.wait = wait || false
    this.offsetReader = new OffsetReader(ks, this.kafkaClient)
    this.values = []
    this.lastSeen = [-2]
  }

  // makeConsumer(ks, id, group) {
  //   const offsetData = ks.idToOffset(id)
  //   return new kafka.ConsumerStream(
  //     ks.getKafkaClient(), [offsetData], {
  //     kafkaHost: ks.kafkaHosts[0],
  //     id: ks.baseGroup,
  //     autoCommit: true,
  //     fromOffset: true,
  //     groupId: group,
  //   })
  //   this.setOffset()
  // } 
  // async *events(): AsyncGenerator<BaseEvent, void, void> {
  //   let resolver = null
  //   const promise = new Promise((resolve, err) => {resolver=resolve})
  //   this.consumer.on('readable', () => {
  //     resolver(true)
  //   });
  //   await promise
  //   let chunk;
  //   console.log(this.consumer)
  //   while (null !== (chunk = this.consumer.read())) {
  //     yield chunk
  //   }
  //   console.log(this.consumer)
  //   this.consumer.close()
  // }

  async atEnd(recheck?: boolean) {
    const knownEnds = await this.offsetReader.lastOffsets(recheck)
    if (!knownEnds)
      throw new Error("Can't happen")
    for (let i = 0; i < knownEnds.length; i++) {
      const last = knownEnds[i]
      if (last > 0 && i < this.lastSeen.length && this.lastSeen[i] < last-1)
        return false
    }
    if (recheck)
      return true
    // recheck
    return await this.atEnd(true)
  }

  async *eventsStartingFromDate(date: Date): AsyncGenerator<Object, void, void> {
    const client = await this.offsetReader.getClient()
    const topic = this.ks.topic
    const promise = new Promise((r)=> {
      client.fetch([{topic: topic, date: date.getTime(), maxNum: 10}], (err, data) => {
        const offsets = data[topic]
        for (const partition of Object.keys(offsets)) {
          const firstEvent = Math.min(...offsets[partition])
          this.consumer.setOffset(topic, partition, firstEvent)
        }
        r()
      })
    })
    await promise
    yield *this.events((evt)=>(evt.ts >= date))
  }

  async *events(filter?: (any)=>boolean): AsyncGenerator<Object, void, void> {
    try {
      let resolver = null
      this.consumer.on('message', (message)=>{
        const id = this.ks.offsetToId(message.partition, message.offset)
        while (message.partition >= this.lastSeen.length) {
          this.lastSeen.push(0)
        }
        this.lastSeen[message.partition] = message.offset
        const payload = JSON.parse(message.value.toString())
        payload['@id'] = id
        payload.ts = new Date(message.timestamp)
        if (filter === undefined || filter(payload)) {
          this.values.push(payload)
          this.promise.resolve(payload)
          this.promise = new PrePromise<?Object>()
        }
      })
      this.consumer.on('error', (error)=>{
        if (!this.ended) {
          this.promise.error(error)
        }
      })
      this.consumer.on('offsetOutOfRange', (error)=>{
        if (!this.ended) {
          this.promise.error(error)
        }
      })
      // this pretty much requires a single-partition topic, since
      // seek is single-partition. Otherwise,
      // I need to use Kafka.Offset.fetch with the date of 1st event?
      this.setOffset()
      while (true) {
        while (this.values.length) {
          yield this.values.shift()
        }
        if (!this.wait && await this.atEnd()) {
          if (this.values.length)
            continue
          this.ended = true
          break
        }
        try {
          await this.promise.promise
        } catch (error) {
          console.error(error)
          break
        }
      }
    } finally {
      this.consumer.close(true)
    }
  }
}

class KafkaSource implements IEventSource {
  spec: IEventStreamSpec
  kafkaHosts: Array<string>
  topic: string
  baseGroup: string
  streamClientCounter: Number
  randomAccessThreads: Set<RandomAccessReaderThread>
  streamThreads: Set<StreamReaderThread>
  offsetReader: OffsetReader
  baseClient: any
  lastId: kId
  // kafka://broker:port,broker:port/topic/groupId
  static kafkaUrlRe = /^kafka:\/\/((?:\w+\:\d+)(?:,\w+\:\d+)*)\/(\w+)\/(\w+)?\/?$/;
  constructor(spec: IEventStreamSpec) {
    const matches: ?Array<string> = spec.data.match(KafkaSource.kafkaUrlRe)
    if (!matches)
      throw new Error("called with invalid spec: "+JSON.stringify(spec))
    this.spec = spec
    this.kafkaHosts = matches[1].split(',')
    this.topic = matches[2]
    this.baseGroup = matches[3] || matches[2]
    this.randomAccessThreads = new Set()
    this.streamThreads = new Set()
    this.baseClient = this.getKafkaClient()
    this.offsetReader = new OffsetReader(this, this.baseClient)
    // this.offsetReader.lastOffsets()
  }

  getKafkaClient() {
    return new kafka.KafkaClient({
      kafkaHost: this.kafkaHosts.join(',')
    })
  }

  idToOffset(id: kId) {
    if (id.startsWith(this.spec.url))
      id = id.substr(this.spec.url.length)
    // else
    //   throw new Error(`${id} does not start with ${this.spec.url}`)
    const id_parts = id.split('.')
    const partition = Number.parseInt(id_parts[0])
    const offset = Number.parseInt(id_parts[1])
    const topic = this.topic
    return {topic, partition, offset}
  }

  offsetToId(partition: number, offset: number): string {
    return `${this.spec.url}${partition}.${offset}`
  }

  // async revivePayload(payload: Object): Promise<?BaseEvent> {
  //     // TODO: can a stream be used by many realms? If so, put realm as arg,
  //     // and wrap the stream in a realm-bound proxy.
  //     let realm = this.realm
  //     // What if realm is implicit? Should eventById give a realm, or return the payload?
  //     if (payload.realm) {
  //       const otherRealm = registry.realmRegistry.getRealm(payload.realm)
  //       if (otherRealm)
  //         realm = otherRealm
  //     }
  //     return await realm.revive(payload)
  // }

  async eventById(id: kId): Promise<?Object> {
    let thread: ?RandomAccessReaderThread = null
    for (const oldthread of this.randomAccessThreads.values()) {
      if (oldthread.id == id || !oldthread.active) {
        thread = oldthread
        break
      }
    }
    if (thread == null) {
      thread = new RandomAccessReaderThread(this, id, this.randomAccessThreads.size)
      this.randomAccessThreads.add(thread)
    }
    return await thread.eventById(id)
  }

  async nextEventId(): Promise<?kId> {
    let lastId: ?kId = this.lastId
    // if (lastId == undefined) {
      const offsets = await this.offsetReader.lastOffsets(true)
      lastId = (offsets[0]>0) ? this.offsetToId(0, offsets[0]) : undefined
      this.lastId = lastId
    // }
    return lastId
  }

  async *eventsStartingFrom(eventId: ?kId): AsyncGenerator<Object, void, void> {
    // kafka blocks if we ask for end of stream
    if (eventId && eventId == await this.nextEventId()) return
    const thread = new StreamReaderThread(this, eventId || '0.0', this.streamThreads.size)
    this.streamThreads.add(thread)
    try {
      yield* thread.events()
    } finally {
      this.streamThreads.delete(thread)
    }
  }

  async *eventsStartingFromDate(date: Date): AsyncGenerator<Object, void, void> {
    const thread = new StreamReaderThread(this, '0.0', this.streamThreads.size)
    this.streamThreads.add(thread)
    try {
      yield* thread.eventsStartingFromDate(date)
    } finally {
      this.streamThreads.delete(thread)
    }
  }

  async shutdown(): Promise<void> {
    for (const thread of this.randomAccessThreads) {
      thread.consumer.close(true, () => {
        this.randomAccessThreads.delete(thread)
      })
    }
    for (const thread of this.streamThreads) {
      try {
        thread.consumer.stop(()=>{
          this.streamThreads.delete(thread)
        })
      } catch (error) {}
    }
    while (this.randomAccessThreads.size || this.streamThreads.size) {
      const p = new Promise(resolve=>setTimeout(resolve, 250))
      await p
    }
  }
  async exists(): Promise<boolean> {
    const promise = new Promise((resolve, err) => {
      this.baseClient.loadMetadataForTopics([this.topic], function (error, results) {
        if (error) {
          err(error)
        } else {
          resolve(results)
        }
      });
    })
    const result = await promise
    const metadata = result[1]['metadata'][this.topic]
    if (metadata) {
      console.debug(metadata)
    }
    return (!!metadata)
  }

}

class KafkaProducer extends KafkaSource implements IEventStream {
  producer: any
  callback: ?IRawEventCallbackFn
  constructor(spec: IEventStreamSpec) {
    super(spec)
    this.producer = new kafka.Producer(this.baseClient)
  }
  async init(): Promise<this> {
    const producerPromise = new Promise((resolve, err) => {
      this.producer.on('ready', ()=>{
        resolve()
      })
    })
    await producerPromise
    return this
  }
  setCallback(cb: IRawEventCallbackFn): void {
    this.callback = cb
  }
  clearCallback(): void {
    this.callback = null
  }
  async appendEvent(event: BaseEvent): Promise<kId> {
    const producer = this.producer
    const value = JSON.stringify(event)
    const key = event.keyId || "command"
    const message = event.keyId? new kafka.KeyedMessage(event.keyId, value) : value;
    const timestamp = event.ts || Date.now()
    const idPromise = new PrePromise<kId>()
    event.idPromise = idPromise.promise

    producer.send([{
      topic: this.topic,
      messages: [message],
      timestamp: timestamp
    }], (err, data) => {
      if (err) {
        idPromise.error(err)
        throw err
      }
      const offsets: [string, any] = Object.entries(data[this.topic])[0]
      const id = this.offsetToId(Number.parseInt(offsets[0]), offsets[1])
      event['@id'] = id
      idPromise.resolve(id)
      if (this.callback)
        this.callback(event)
    })
    return idPromise.promise
  }
  async resetEvents(until_event: ?kId): Promise<void> {
    if (until_event)
      throw new Error("Not partially resettable")
    await this.deleteTopic()
  }

  async createTopic(nocheck: ?boolean): Promise<void> {
    if (!nocheck || await this.exists()) return
    // TODO: Make zookeeper part of specification, or extend kafka-node to take config into account.
    const cmd: string = `kafka-topics --zookeeper localhost:2181 --create --topic ${this.topic} --partitions 1 --replication-factor 1 --config retention.bytes=-1 --config retention.ms=-1`
    return exec(cmd)
  }
  async deleteTopic(): Promise<void> {
    const cmd: string = `kafka-topics --zookeeper localhost:2181 --delete --topic ${this.topic}`
    try {
      await exec(cmd)
    } catch (err) {
      console.error(err)
    }
  }

  isResettable(): boolean {
    return false;
  }
  async shutdown(): Promise<void> {
    try {
      const producer = this.producer
      producer.disconnect()
    } catch (error) {}
    await super.shutdown()
  }
}

class StreamRegistry {
    kafkaStreams: Array<KafkaSource>
    kafkaDisconnectPromises: Array<Promise<void>>
    realmRegistry: IRealmRegistry

    constructor(realmRegistry: IRealmRegistry) {
      this.realmRegistry = realmRegistry
      this.kafkaStreams = []
      this.kafkaDisconnectPromises = []
    }

    async getStream(spec: IEventStreamSpec, nocreate: ?boolean): Promise<?KafkaSource> {
      let source: ?KafkaSource
      try {
        source = new KafkaProducer(spec)
        if (source) {
          const exists = await source.exists()
          if (!nocreate && !exists)
            await source.createTopic(true)
          await source.init()
          this.kafkaStreams.push(source)
        }
      } catch (error) {
        console.error(error)
      }
      return source
    }

    async shutdown(): Promise<Array<void>> {
      const newPromises: Array<Promise<void>> = this.kafkaStreams.map((s) => s.shutdown())
      this.kafkaDisconnectPromises = this.kafkaDisconnectPromises.concat(newPromises)
      return Promise.all(this.kafkaDisconnectPromises)
    }
}

let registry: StreamRegistry

process.on('SIGINT', function() {
    registry.shutdown().then(()=>{
        process.exit();
    })
});
async function KafkaEventStreamFactory(spec: IEventStreamSpec, nocreate: ?boolean): Promise<?IEventStream> {
  const matches: ?Array<string> = spec.data.match(KafkaSource.kafkaUrlRe)
  if (matches) {
    const stream: ?KafkaSource = await registry.getStream(spec, nocreate)
    return stream
  }
}
export function init(realmRegistry: IRealmRegistry) {
  registry = new StreamRegistry(realmRegistry)
  realmRegistry.registerEventStreamFactory('kafka', KafkaEventStreamFactory)
}

