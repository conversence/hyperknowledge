// @flow
// import rp from "request-promise";
import jsonld from "jsonld";
import jsonld_context from "jsonld/lib/context";
import jsonld_compact from "jsonld/lib/compact";
import { base_jsonld, base_jsonld_url } from "./vocabulary";
import type { Context, context_mapping_entry, contextEntry, contextEntryD, localContext,
  localContextSeq, DocumentLoader, DocumentLoaderResult } from "jsonld";
import type { kName, kId, kGlobalId } from './types';

function _localEntry(o: context_mapping_entry): contextEntry {
    if (o['@type'] || o.reverse || o._prefix) {
        const r: contextEntryD = {};
        if (o['@type'])
          r['@type'] = o['@type'];
        if (o.reverse)
          r['@reverse'] = o['@id'];
        else
          r['@id'] = o['@id']
        if (o._prefix)  r['@prefix'] = true;
        return r;
    }
    return o['@id'];
}

export class JLDContext {
  base: ?string;
  url: ?kGlobalId;
  base_ctx: ?JLDContext;
  context: Context;
  _ctx_data: ?localContextSeq;
  static documentLoader: DocumentLoader = jsonld.documentLoader;

  constructor(data: ?localContextSeq, url: ?kGlobalId, base?: ?string, base_ctx?: JLDContext) {
    this.base = base
    this._ctx_data = data
    if (!url) {
      if (!data)
        throw new Error("Context must have a URL or context object")
      if (typeof(data) == 'string')
        url = data
    }
    this.url = url
    this.base_ctx = base_ctx
  }

  async init() {
    let ctx: ?localContextSeq = this._ctx_data
    if (Array.isArray(ctx)) {
      if (ctx.length == 1) {
        ctx = ctx[0]
      } else {
        if (this.base_ctx)
          throw new Error("Cannot initialize complex context if base is given")
        const lastRef = ctx.pop()
        this.base_ctx = new JLDContext(ctx)
        await this.base_ctx.init()
        ctx = lastRef
      }
    }
    let base_ctx: Context;
    if (this.base_ctx) {
      base_ctx = this.base_ctx.context
    } else {
      base_ctx = jsonld_context.getInitialContext({base: this.base})
    }

    if (!ctx) {
      // we know this.url != null, but keep flow happy
      this.context = await jsonld.processContext(base_ctx, this.url || "", {documentLoader: JLDContext.documentLoader})
      // OR
      // ctx = await jsonld.get(this.url, {documentLoader: jsonld.documentLoader})
      // ctx = ctx.document
      // OR
      // ctx = await rp.get({ uri: this.url, json: true })
      // if (ctx['@context'])
      //   ctx = ctx['@context']
    } else
      this.context = await jsonld.processContext(base_ctx, ctx, {documentLoader: JLDContext.documentLoader})
    if (!this.base) {
      const base = jsonld.url.prependBase(this.context['@base'], '')
      if (base && base.length > 2)  // empty base...
        this.base = base
    }
    return this
  }
  expandIri(iri: kName, vocab?: boolean): kId {
    if (!iri) throw new Error("expandIri null?")
    let maybeExpanded = jsonld_context.expandIri(this.context, iri, {vocab: true, base: false})
    if (maybeExpanded == iri)
      maybeExpanded = jsonld_context.expandIri(this.context, iri, {vocab: true, base: true})
    return maybeExpanded
  }
  compactIri(iri: kId): kName | kId {
    if (!iri) throw new Error("compactIri null?")
    let maybeCompacted = jsonld_compact.compactIri({
      activeCtx: this.context,
      iri: iri,
      relativeTo: {vocab: true} })
    if (maybeCompacted == iri) {
      maybeCompacted = jsonld_compact.compactIri({
        activeCtx: this.context,
        iri: iri,
        relativeTo: {vocab: false} })
    }
    return maybeCompacted
  }
  asContext(allowUrl?: boolean): localContextSeq {
    let contexts: localContextSeq
    if (this.base_ctx) {
      contexts = this.base_ctx.asContext(true)
      if (!Array.isArray(contexts))
        contexts = [contexts]
    } else {
      contexts = []
    }
    if (allowUrl && this.url) {
      if (contexts.length) {
        contexts.push(this.url)
        return contexts
      } else {
        return this.url
      }
    }
    const asLocal: Object = {}
    const mappings = this.context.mappings;
    const myBase = this.base
    if (myBase) {
      asLocal['@base'] = myBase
    }
    if (this.context['@vocab']) {
      asLocal['@vocab'] = this.context['@vocab']
    }
    for (const k: string of mappings.keys()) {
      const base_ctx = this.base_ctx
      if (base_ctx && base_ctx.hasTerm(k)) {
        const o1 = this.context.mappings.get(k)
        const o2 = base_ctx.context.mappings.get(k)
        if (o1 && o2 && (o1['@id'] == o2['@id']) && (o1['@type'] == o2['@type'])
            && (o1._prefix == o2._prefix) && (o1.reverse == o2.reverse))
          continue;
      }
      const entry = mappings.get(k)
      if (entry)
        asLocal[k] = _localEntry(entry)
    }
    if (Object.keys(asLocal).length) {
      if (this.context.processingMode == 'json-ld-1.1')
        asLocal['@version'] = 1.1
      contexts.push(asLocal)
    }
    if (contexts.length == 1) {
      return contexts[0]
    } else {
      return contexts
    }
  }
  hasTerm(term: kName): boolean {
    return this.context.mappings.has(term)
  }
  get base(): ?kId {
    if (this.context['@base']) {
      const base = jsonld.url.prependBase(this.context['@base'], '')
      if (base && base.length)
        return base
    }
  }
  static contextUrlFromResponse(response: Object): ?kGlobalId {
    let links = response.headers.link
    if (!links) return
    if (!Array.isArray(links)) {
      links = [links]
    }
    for (const link of links) {
      if (link.endsWith('; rel="http://www.w3.org/ns/json-ld#context"')) {
        const base = link.split('>')[0]
        if (base.charAt(0) == '<')
          return base.substring(1)
      }
    }
  }
  static async contextFromResponse(response: Object): Promise<?JLDContext> {
    const url = await JLDContext.contextUrlFromResponse(response)
    let body: Object = response.data
    const mimetype = response.headers['content-type']
    if (typeof(body) == 'string' && (mimetype.startsWith('application/ld+json') || mimetype.startsWith('application/json')))
      body = JSON.parse(body)
    const ctxData = body['@context'] || url
    if (ctxData) {
      const context = new JLDContext(ctxData, url)
      return await context.init()
    }
  }

  addTerm(term: kName, id: kId) {
    const ctx = {}
    ctx[term] = id
    jsonld_context.createTermDefinition(this.context, ctx, term, new Map())
  }
  removeTerm(term: kName) {
    this.context.mappings.delete(term)
    this.context.inverse = null;
  }

}

/**
Optimization: Do not hit the net for the base context.
*/
if (base_jsonld) {
  const base_jsonld_str = JSON.stringify(base_jsonld)
  async function localJsonLdLoader(url: string): Promise<DocumentLoaderResult> {
     if (url == base_jsonld_url) {
       return {contextUrl: null, documentUrl: base_jsonld_url, document: base_jsonld_str}
     }
    return await jsonld.documentLoader(url)
  }
  JLDContext.documentLoader = localJsonLdLoader
}
