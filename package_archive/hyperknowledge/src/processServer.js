// @flow

import {
  type kId,
  type kGlobalId,
  type kName,
  type URLType,
  type kRealmId
} from "./types";
import { LocalRealm } from "./realms";
import {
  RealmRegistry,
  BaseEvent,
  Realm,
  Process
} from "./baseTopics";

export class Continuation {
  date: Date;
  done: boolean;
  gen: AsyncGenerator<BaseEvent, void, BaseEvent[]>;
  constructor(gen: AsyncGenerator<BaseEvent, void, BaseEvent[]>) {
    this.date = new Date();
    this.gen = gen;
    this.done = false;
  }
  stop() {
    if (!this.done) this.gen.throw();
    this.done = true;
  }
}

export class Continuations {
  process: Process;
  max_duration: number;
  continuations: Map<string, Continuation>;
  constructor(process: Process, max_duration: number = 60 * 60 * 1000) {
    this.process = process;
    this.max_duration = max_duration;
    this.continuations = new Map();
  }
  cleanup() {
    const now = new Date();
    const elapsed = [...this.continuations.keys()].filter(
      d => now - new Date(d) > this.max_duration
    );
    for (const d of elapsed) {
      const continuation = this.continuations.get(d);
      if (continuation) continuation.stop();
      this.continuations.delete(d);
    }
  }

  add(continuation: Continuation) {
    this.cleanup();
    const dateString = continuation.date.toISOString();
    this.continuations.set(dateString, continuation);
    return dateString;
  }

  stop(continuation: Continuation) {
    continuation.stop();
    this.continuations.delete(continuation.date.toISOString());
    this.cleanup();
  }

  get(id: string): ?Continuation {
    return this.continuations.get(id);
  }
}
