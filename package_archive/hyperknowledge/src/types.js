// @flow
export type kName = string; // topic name, mostly used for types. Mapping to kId is realm-specific.
export type kId = string; // topic id. We could use ints.
export type URLType = string;
export type kIdSA = kId | kId[];
export type kGlobalId = kId; // A kId that can be resolved without giving the kRealmId
export type kRealmId = kGlobalId; // unlike kIds, realmIds are global (probably URLs.)
export type kAccessSpec = URLType; // URL that tells how to access a resource (event stream, topic source, etc.)
