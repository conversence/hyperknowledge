// @flow
import "@babel/polyfill";
export function update_set<T>(s: Set<T>, i: Iterable<T>): void {
  for (const e: T of i) {
    s.add(e);
  }
}

export function* chain<T>(
  ...iterables: (?Iterable<T>)[]
): Generator<T, void, void> {
  for (let iter: ?Iterable<T> of iterables) {
    if (iter) yield* iter;
  }
}

export function strMapToObj<K, V>(strMap: Map<K, V>): { [key: K]: V } {
  let obj: { [key: K]: V } = {};
  for (let [k: K, v: V] of strMap) {
    // We don’t escape the key '__proto__'
    // which can cause problems on older engines
    obj[k] = v;
  }
  return obj;
}

export function objToStrMap<K, V>(obj: { [key: K]: V }): Map<K, V> {
  let strMap: Map<K, V> = new Map();
  for (let k of Object.keys(obj)) {
    strMap.set(k, obj[k]);
  }
  return strMap;
}
export function sleep(m: number): Promise<void> {
  return new Promise((r) => setTimeout(r, m))
}

export const in_nodejs: boolean = (
  typeof process !== 'undefined' && !!process.versions && !!process.versions.node);
