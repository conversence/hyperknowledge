// @flow
import rp from "request-promise-native";

import {
  TopicXRef,
  BaseTopic,
  RealmRegistry,
  Relation,
  SimpleRelation,
  BaseEvent,
  hasError,
  Concept,
  Realm
} from "../src/baseTopics.js";
import type {IRelation}  from "../src/baseTopics.js"
import {
  CreateTopicDCommand,
  AddRelationDCommand,
  ImportTopicDCommand,
  SyncRealmDCommand,
  CommandProcessor
} from "../src/derivedCommands.js";
import { LocalRealm } from "../src/realms.js";
import { AddAttributeCommand, extractTargetId } from "../src/baseEvents.js";
import { JLDContext } from "../src/jsonld.js";
import { base_jsonld_url } from "../src/vocabulary.js";
const registry = RealmRegistry;
let localRealm: LocalRealm;
let localRealm2: LocalRealm;

beforeAll(async () => {
  await registry.initBaseTypes();
  RealmRegistry.registerProcess(new CommandProcessor());
  localRealm = new LocalRealm("http://example.com/first/", {
    name: "first",
    type: "mem",
    prefix: "ev:",
    url: "http://example.com/evs/first/"
  });
  await localRealm.init()
  registry.registerRealm(localRealm);
  localRealm2 = new LocalRealm("http://example2.com/second/", {
    name: "second",
    type: "mem",
    prefix: "ev:",
    url: "http://example.com/evs/second/"
  });
  await localRealm2.init()
  registry.registerRealm(localRealm2);
});

beforeEach(() => {
  localRealm.reset();
  localRealm2.reset();
});

test("Init registry", () => {
  expect(registry).toBeDefined();
  expect(localRealm).toBeDefined();
  // console.log(registry)
});

test("Jsonld Context class", async () => {
  const c1 = new JLDContext({})
  await c1.init()
  const newdefs = {'example': 'http://example.com'}
  c1.addTerm('example', 'http://example.com')
  expect(c1.asContext()['example']).toEqual(newdefs['example'])

  const c2 = new JLDContext(base_jsonld_url)
  await c2.init()
  // console.log(c2.asContext())
  expect(c2.expandIri('Topic')).toBe('http://purl.org/hyperknowledge/topic#Topic')
  expect(c2.compactIri('http://purl.org/hyperknowledge/topic#Topic')).toBe('Topic')
  expect(c2.expandIri('http://unrelated.com/x')).toBe('http://unrelated.com/x')
  expect(c2.compactIri('http://unrelated.com/x')).toBe('http://unrelated.com/x')

  const c3 = new JLDContext({}, null, localRealm.id)
  await c3.init()
  expect(c3.expandIri('events/1')).toBe('http://example.com/first/events/1')
  expect(c3.compactIri('http://example.com/first/events/1')).toBe('events/1')
});


async function genToList<T>(gen: AsyncGenerator<T, void, void>): Promise<Array<T>> {
  const l: Array<T> = []
  for await (const c: T of gen) l.push(c)
  return l
}

test("Base classes", async () => {
  expect(Realm.inherentTypeC()).toBeDefined()
  const realmClass = registry.glob.Realm
  expect(realmClass).toBeDefined()
  expect(Realm.inherentTypeC()).toBe(realmClass)
  const subClassOf = registry.glob.subClassOf
  expect(subClassOf).toBeDefined()
  const source = registry.glob.source
  expect(source).toBeDefined()
  const target = registry.glob.target
  expect(target).toBeDefined()
  const Process = registry.glob.Process
  expect(Process).toBeDefined()
  const Concept = registry.glob.Concept
  expect(Concept).toBeDefined()
  const Topic = registry.glob.Topic
  expect(Topic).toBeDefined()
  const realmSuperClassRels: Array<IRelation> = await genToList<IRelation>(realmClass.inRelations(null, null, true))
  // console.log(realmSuperClassRels.map(x=>`[${x.type.name}, ${x.source.type.name}:${x.source.actorRef.id} ${x.target.type.name}:${x.target.actorRef.id}]`))
  const realmSuperClassSubRels: Array<IRelation> = await genToList<IRelation>(realmClass.inRelations(subClassOf, source, true))
  // console.log(realmSuperClassSubRels)
  expect(realmSuperClassSubRels.length).toBeGreaterThan(0)
  const realmSuperClassActors: Array<BaseTopic> = await genToList<BaseTopic>(realmClass.relatedConcepts(subClassOf, source, target, true))
  expect(realmSuperClassActors.length).toBeGreaterThan(0)
  expect(realmSuperClassActors).toContain(Process)

  const realmSuperClasses = new Set()
  realmSuperClasses.add(realmClass)
  await realmClass.relationClosure(realmSuperClasses, subClassOf, source, target, true)
  // console.log(realmSuperClasses)
  expect(realmSuperClasses.size).toBeGreaterThan(3)
  expect(realmSuperClasses).toContain(realmClass)
  expect(realmSuperClasses).toContain(Process)
  expect(realmSuperClasses).toContain(Concept)
  expect(realmSuperClasses).toContain(Topic)
})

test("Add Concept", async () => {
  let command = new CreateTopicDCommand(localRealm, "RelationMetaClass");
  let result = await localRealm.applyCommand(command);
  expect(hasError(result.values())).toBeUndefined();
  const topicId = await extractTargetId(command);
  expect(topicId).toBeDefined();
  if (!topicId) return;
  let concept = await localRealm.byId(topicId);
  expect(concept).toBeDefined();
  // console.log(JSON.stringify(concept));
  const revived = await localRealm.revive(JSON.parse(JSON.stringify(concept)));
  // console.log(JSON.stringify(revived));
});

test("Add Attribute", async () => {
  let command: BaseEvent = new CreateTopicDCommand(localRealm);
  let result = await localRealm.applyCommand(command);
  expect(hasError(result.values())).toBeUndefined();
  const topicId = await extractTargetId(command);
  expect(topicId).toBeDefined();
  if (!topicId) return;
  let concept = await localRealm.byId(topicId);
  expect(concept).toBeDefined();
  command = new AddAttributeCommand(
    localRealm,
    topicId,
    new TopicXRef(
      RealmRegistry.vocabulary.title,
      RealmRegistry.rootRealm),
    null,
    "this is a label"
  );
  result = await localRealm.applyCommand(command);
  expect(hasError(result.values())).toBeUndefined();
  expect(concept).toBeInstanceOf(Concept);
  if (concept instanceof Concept) {
    expect(concept.attributes.size).toBe(1);
    // console.log(JSON.stringify(concept));
  }
});

test("Add Relation", async () => {
  let command = new CreateTopicDCommand(localRealm);
  let result = await localRealm.applyCommand(command);
  expect(hasError(result.values())).toBeUndefined();
  const conceptAId = await extractTargetId(command);
  expect(conceptAId).toBeDefined();
  if (!conceptAId) return;
  command = new CreateTopicDCommand(localRealm);
  result = await localRealm.applyCommand(command);
  expect(hasError(result.values())).toBeUndefined();
  const conceptBId = await extractTargetId(command);
  expect(conceptBId).toBeDefined();
  if (!conceptBId) return;
  command = new AddRelationDCommand(localRealm, null, [
    { roleName: "source", actor: localRealm.compactIri(conceptAId) },
    { roleName: "target", actor: localRealm.compactIri(conceptBId) }
  ]);
  result = await localRealm.applyCommand(command);
  expect(hasError(result.values())).toBeUndefined();
  const conceptRId = await extractTargetId(command);
  expect(conceptRId).toBeDefined();
  if (!conceptRId) return;
  const rel = await localRealm.byId(conceptRId);
  expect(rel instanceof SimpleRelation || rel instanceof Relation).toBe(true);

  if (rel instanceof SimpleRelation) {
    expect(rel.source).toBeDefined();
    expect(rel.target).toBeDefined();
  } else if (rel instanceof Relation) {
    expect(rel._bindings.size).toBe(2);
  }
});

test("Import Concept Local to Local", async () => {
  let command = new CreateTopicDCommand(localRealm);
  let result = await localRealm.applyCommand(command);
  expect(hasError(result.values())).toBeUndefined();
  const conceptAId = await extractTargetId(command);
  expect(conceptAId).toBeDefined();
  if (!conceptAId) return;
  command = new ImportTopicDCommand(
    localRealm2,
    new TopicXRef(conceptAId, localRealm)
  );
  expect(localRealm2.concepts.size).toBe(0);
  result = await localRealm2.applyCommand(command);
  expect(hasError(result.values())).toBeUndefined();
  expect(localRealm2.concepts.size).toBe(1);
  // console.log(localRealm2);
});

test("Sync realms", async () => {
  let command = new CreateTopicDCommand(localRealm);
  let result = await localRealm.applyCommand(command);
  expect(hasError(result.values())).toBeUndefined();
  const conceptAId = await extractTargetId(command);
  expect(conceptAId).toBeDefined();
  if (!conceptAId) return;
  command = new CreateTopicDCommand(localRealm);
  result = await localRealm.applyCommand(command);
  expect(hasError(result.values())).toBeUndefined();
  const conceptBId = await extractTargetId(command);
  expect(conceptBId).toBeDefined();
  if (!conceptBId) return;
  command = new AddRelationDCommand(localRealm, null, [
    { roleName: "source", actor: localRealm.compactIri(conceptAId) },
    { roleName: "target", actor: localRealm.compactIri(conceptBId) }
  ]);
  result = await localRealm.applyCommand(command);
  expect(hasError(result.values())).toBeUndefined();
  command = new AddAttributeCommand(
    localRealm,
    conceptAId,
    new TopicXRef(
      RealmRegistry.vocabulary.title,
      RealmRegistry.rootRealm),
    null,
    "this is a label"
  );
  result = await localRealm.applyCommand(command);
  expect(hasError(result.values())).toBeUndefined();
  expect(localRealm2.concepts.size).toBe(0);
  command = new SyncRealmDCommand(localRealm2, localRealm.id, true, true);
  result = await localRealm2.applyCommand(command);
  expect(hasError(result.values())).toBeUndefined();
  let json = await localRealm.topicJSON();
  // console.log(JSON.stringify(json));
  json = await localRealm.eventsJSON();
  // console.log(JSON.stringify(json));
  json = await localRealm2.topicJSON();
  // console.log(JSON.stringify(json));
  json = await localRealm2.eventsJSON();
  // console.log(JSON.stringify(json));
  const remoteToLocalIdMap = localRealm2.remoteToLocalIdCache.get(
    localRealm.id
  );
  expect(remoteToLocalIdMap).toBeDefined();
  if (!remoteToLocalIdMap) return;
  for (const concept_id of localRealm.concepts.keys()) {
    const localId = remoteToLocalIdMap.get(concept_id);
    // console.log(`sync: ${concept_id} -> ${localId || 'null'}`)
    expect(localId).toBeDefined();
  }
  // console.log(localRealm2);
});
