var path = require('path');
const FlowWebpackPlugin = require('flow-webpack-plugin')
const babel = require("@babel/core")
const MinifyPlugin = require("babel-minify-webpack-plugin");

module.exports = {
  entry: [
    './src/index.js'
  ],
  output: {
    filename: 'hyperknowledge_web.js',
    libraryTarget: 'var',
    library: 'hyperknowledge',
    publicPath: '/',
    path: path.resolve(__dirname, 'dist')
  },
  devtool: 'source-map',
  plugins: [
    new FlowWebpackPlugin(),
    // new MinifyPlugin({ removeUndefined: false, mangle: false }, {babel: babel, sourceMap: false}),
  ],
  target: 'web',
  externals: {
    'fs': 'fs',
    'request': 'request',
    'tunnel-agent': 'tunnel-agent',
    'forever-agent': 'forever-agent',
    'tough-cookie': 'tough-cookie',
    'request-promise-native': 'request-promise-native',
  },
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        options: {
          "presets": [
            "@babel/preset-flow",
            [
              "@babel/preset-env",
              {
                "targets": {
                  "browsers": [ ">0.25%", "not dead"]
                }
              }
            ]
          ]
        }
      }
    ]
  }
}
