// @flow
export type kName = string; // topic name, mostly used for types. Mapping to kId is realm-specific.
export type localId = string; // topic id.
export type eventId = string; // event id.
export type eventLocator = eventId | Date;
export type URLType = string;
export type kGlobalId = string; // A kId that can be resolved without giving the kRealmId
export type kRealmId = kGlobalId; // unlike kIds, realmIds are global (probably URLs.)
export type kAccessSpec = URLType; // URL that tells how to access a resource (event stream, topic source, etc.)

export const presenceTypes = {
    created: "created",   // the topic was created in this perspective
    modified: "modified",  // the topic was altered in this perspective
    transcluded: "transcluded",  // the topic was transcluded in this perspective
}

export type PresenceType = $Keys<typeof presenceTypes>;
