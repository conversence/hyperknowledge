import type { uri, Json, JsonSerializable, JsonCompatible } from './types';
import { BaseEvent } from './events';

class Agent implements JsonSerializable<Agent> {
  readonly id: uri;
  constructor() {
    //TODO
    this.id = '';
  }
  sign(content: Json): string {
    return 'TODO';
  }
  toJSON(): JsonCompatible<Agent> {
    return {
      id: this.id,
    };
  }
}

export class OwnedEvent<T extends JsonSerializable<T>> extends BaseEvent<T> {
  readonly who: Agent;
  constructor(event: BaseEvent<T>, who: Agent) {
    super(event.stream, event.type, event.what, event.when, event.id);
    this.who = who;
  }
  toJSON(withoutId?: boolean): JsonCompatible<OwnedEvent<T>> {
    const json: any = super.toJSON(withoutId);
    json.assign({ who: this.who.id });
    return json;
  }
}

export class SignedEvent<T extends JsonSerializable<T>> extends OwnedEvent<T> {
  readonly signature: string;
  constructor(event: BaseEvent<T>, who: Agent, signature: string) {
    super(event, who);
    this.signature = who.sign(this.toJSON(true));
  }
  static fromEvent<U extends JsonSerializable<U>>(
    event: BaseEvent<U>,
    agent: Agent,
  ): SignedEvent<U> {
    const json: any = event.toJSON();
    json['who'] = agent.id;
    const signature = agent.sign(json);
    return new SignedEvent<U>(event, agent, signature);
  }
  toJSON(
    withoutId?: boolean,
    withoutSig?: boolean,
  ): JsonCompatible<SignedEvent<T>> {
    const json: any = super.toJSON(true);
    if (!withoutSig) {
      json['sig'] = this.signature;
    }
    if (!withoutId) {
      json['@id'] = this.id;
    }
    return json;
  }
}
