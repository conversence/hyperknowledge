import { BaseEvent, BaseStream } from './events';
import type { eventId, uri, typeUri, JsonSerializable } from './types';

export class TopicEvent<T extends JsonSerializable<T>> extends BaseEvent<T> {
  readonly topics: uri[];
  constructor(
    stream: BaseStream,
    type: typeUri,
    what: T,
    topics: uri[],
    when?: Date,
    id?: eventId,
  ) {
    super(stream, type, what, when, id);
    this.topics = topics;
  }
}
