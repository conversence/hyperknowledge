import 'core-js/modules/es7.symbol.async-iterator';

export type uri = string;
export type streamUri = uri;
export type typeUri = uri;
export type eventId = string | number;
export type eventSpec = eventId | Date;

// h/t https://github.com/microsoft/TypeScript/issues/1897#issuecomment-580962081
export type Json =
  | void
  | Date
  | null
  | boolean
  | number
  | string
  | Json[]
  | { [prop: string]: Json };

export type JsonCompatible<T> = {
  [P in keyof T]?: T[P] extends Json
    ? T[P]
    : Pick<T, P> extends Required<Pick<T, P>>
    ? never
    : T[P] extends (() => any) | undefined
    ? never
    : JsonCompatible<T[P]>;
};

export type JsonCompatibleR<T> = {
  [P in keyof T]: T[P] extends Json
    ? T[P]
    : Pick<T, P> extends Required<Pick<T, P>>
    ? never
    : T[P] extends (() => any) | undefined
    ? never
    : JsonCompatibleR<T[P]>;
};

export interface JsonSerializable<T> {
  toJSON: () => JsonCompatible<T>;
}
